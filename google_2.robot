*** Settings ***
Library     Selenium2Library
Library     Collections

*** Variables ***
${browser}     gc
${url}     https://www.google.co.th
${text_search}     cat

*** Test Cases ***
1_Open_Web_Browser
     Open_Web_Browser

2_Search_by_Text
    Search_by_Text     ${text_search}

3_Close_Web_Browser
     Close_Web_Browser


*** Keywords ***
Open_Web_Browser
     Open Browser     ${url}     ${browser}
     Sleep     2s


Search_by_Text
     [Arguments]     ${text_search}

        Input Text     q     ${text_search}
        Sleep     3s
        Click Button     btnK
        Sleep     3s
        Capture Page Screenshot

Close_Web_Browser
    Close Browser
